def fizzbuzzpopper(number):
    my_dict = {3:'Fizz', 5:'Buzz', 7:'Pop'}
    result = ''

    for key in sorted(my_dict):
        if number%key == 0:
            result += str(my_dict[key])
    if result == '':
        result += str(number)
    return result

def testFB(number):
    for i in range(1, number+1, 1):
        print(i, fizzbuzzpopper(i), sep =" : ")

number = input("Input the number up to which you wish to see Fizz-Buzz-Poping! :  ")
testFB(int(number))
